package com.example.marius.connection.model;

public class User {

    private int id;
    private String name;
    private String sex;
    private int year;

    public User(int id, String name, String sex, int year) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public int getYear() {
        return year;
    }
}
